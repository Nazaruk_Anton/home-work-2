# -*- coding: utf-8 -*-

import csv
import argparse
import os
from operator import itemgetter


def main():
  parser = argparse.ArgumentParser()
  parser.add_argument('-n', '--n', type=int, default=-1,
                      help='How many top movies do you want to display?')
  parser.add_argument('-g', '--genres', type=str, default=None,
                      help='Specify genres: Comedy or Comedy|Adventure or Comedy&Adventure')
  parser.add_argument('-yf', '--year_from', type=int, default=None,
                      help='Select movies starting from specific year')
  parser.add_argument('-yt', '--year_to', type=int, default=None,
                      help='Select movies up to (including) specified year')
  parser.add_argument('-r', '--regexp', type=str, default=None,
                      help='Select movies where regexp fits movie title, not case sensitive')
  args = parser.parse_args()

  ratings_path = os.path.join(os.getcwd(), 'Data', 'ratings.csv')
  movies_path = os.path.join(os.getcwd(), 'Data', 'movies.csv')
  movies_dict, ratings_lst, bad_data_lst = parse_data_from_csv(ratings_path, movies_path)
  movies_dict = clean_and_merge_data(movies_dict, ratings_lst, bad_data_lst)

  if args.year_from and args.year_to:
    movies_dict = year_from_to(movies_dict, args.year_from, args.year_to)
  elif args.year_from:
    movies_dict = year_from_to(movies_dict, year_from=args.year_from)
  elif args.year_to:
    movies_dict = year_from_to(movies_dict, year_to=args.year_to)
  if args.regexp:
    movies_dict = regexp(args.regexp, movies_dict)
  if not args.genres:
    movies_dict = top_movies_by_all_genres(movies_dict)
  if args.genres and '&' in args.genres:
    genres_lst = [genre.lower() for genre in args.genres.split('&')]
    movies_dict = top_movies_by_combined_genres(genres_lst, movies_dict)
  if args.genres and '|' in args.genres:
    genres_lst = [genre.lower() for genre in args.genres.split('|')]
    movies_dict = top_movies_by_specific_genres(genres_lst, movies_dict)
  if args.genres and '&' not in args.genres and '|' not in args.genres:
    genres_lst = [genre.lower() for genre in args.genres.split()]
    movies_dict = top_movies_by_specific_genres(genres_lst, movies_dict)
  
  display_results(movies_dict, args.n)


def parse_data_from_csv(ratings_csv, movies_csv):

  """Opens and reads csv files with movies descriptions and users ratings.
     Finds data entries that do not fit common pattern"""

  movies_dict = {}
  bad_data_lst = []
  # transform movies_csv to python dictionary
  with open(movies_csv) as f:
    csv_reader = csv.reader(f)
    next(csv_reader)

    for movie in csv_reader:
      year = movie[1][-5:-1] 
      genres = movie[-1]
      title = movie[1][:-7]
      movie_id = movie[0]
      # check if data fits common pattern
      if year.isdigit() and genres:
        movies_dict[movie_id] = [title, int(year), genres.split('|')]  
      else:
        bad_data_lst.append(movie)

  # transform ratings_csv to python list
  with open(ratings_csv) as f:
    ratings_csv = csv.reader(f)
    next(ratings_csv)
    ratings_lst = list(ratings_csv)

    # drop useless data
    for i in ratings_lst:
      del i[0]
      del i[-1]

  return movies_dict, ratings_lst, bad_data_lst


def clean_and_merge_data(movies_dict, ratings_lst, bad_data_lst):
  
  """Removes bad data, calculates average rating and
     merges movies descriptions with ratings into one dictionary"""

  ratings_dict = {}
  # group ratings by movie_id in dictionary
  for movie_id_rating_pair in ratings_lst:
    movie_id = movie_id_rating_pair[0]
    rating = movie_id_rating_pair[1]
    if ratings_dict.get(movie_id):
      ratings_dict[movie_id].append(float(rating))
    else:
      ratings_dict[movie_id] = []
      ratings_dict[movie_id].append(float(rating))

  # remove data entries that do not fit common pattern
  for i in bad_data_lst:
    corrupted_movie_id = i[0]
    del ratings_dict[corrupted_movie_id]

  # calculate average rating
  for movie_id in ratings_dict:
    ratings_dict[movie_id] = sum(ratings_dict[movie_id]) / len(ratings_dict[movie_id])
  
  # merge movies descriptions with ratings
  for movie_id in ratings_dict:
    movies_dict[movie_id].append(ratings_dict[movie_id])

  # movies without user rating get 0 instead 
  for key, value in movies_dict.items():
    if len(value) == 3:
      movies_dict[key].append(0)

  return movies_dict

# %%time
# movies_dict, ratings_lst, bad_data_lst = parse_data_from_csv('/content/ratings.csv', '/content/movies.csv')
# clean_and_merge_data(movies_dict, ratings_lst, bad_data_lst)


def top_movies_regardless_genres(movies_dict, n=-1):

  """Makes a list of all movies sorted by rating then by year then by title"""

  movies_lst_sorted_by_title = sorted(movies_dict.values(), key=itemgetter(0))
  movies_lst_sorted_by_everything = sorted(movies_lst_sorted_by_title, key=itemgetter(3, 1), reverse=True)
  return movies_lst_sorted_by_everything[0:n]


def regexp(query, movies_dict):

  """Selects only those movies where title meets query"""

  query = query.lower()
  movies_filtered_by_name = {}
  for movie_id in movies_dict:
    movie_title = movies_dict[movie_id][0].lower()
    if query in movie_title:
      movies_filtered_by_name[movie_id] = movies_dict[movie_id]
  return movies_filtered_by_name


def year_from_to(movies_dict, year_from=0, year_to=2100):

  """Selects only those movies where year is between year_from and year_to"""

  movies_filtered_by_year = {}
  for i in movies_dict:
    year = movies_dict[i][1] 
    if year >= year_from and year <= year_to:
      movies_filtered_by_year[i] = movies_dict[i]
  return movies_filtered_by_year


def top_movies_by_all_genres(movies_dict):

  """Makes a dictionary where key is one of EVERY genre listed in data set and value
     is a list of movies of this specific genre sorted by rating then by year then by title"""

  # create list of all genres
  genres_lst = []
  for movie_id in movies_dict:
    for genre in movies_dict[movie_id][2]:
      if genre not in genres_lst:
        genres_lst.append(genre)

  # create dictionary where every key is a genre and every value
  # is a list of movies of specific genre
  tmp_genres_dict = {}
  for genre in genres_lst:
    tmp_genres_dict[genre] = []
  for movie_id in movies_dict:
    list_of_genres_for_specific_movie = movies_dict[movie_id][2]
    for genre in list_of_genres_for_specific_movie:
      tmp_genres_dict[genre].append(movies_dict[movie_id])

  # sort movies by rating then by year then by title for every genre
  genres_dict = {}
  for genre in tmp_genres_dict:
    genres_lst_sorted_by_title = sorted(tmp_genres_dict[genre], key=itemgetter(0))
    genres_lst_sorted_by_everything = sorted(genres_lst_sorted_by_title, key=itemgetter(3, 1), reverse=True)
    genres_dict[genre] = genres_lst_sorted_by_everything

  return genres_dict


def top_movies_by_specific_genres(genres_lst, movies_dict):

  """Makes a dictionary where key is a genre from genres_lst and value
     is a list of movies of this specific genre sorted by rating then by year then by title"""

  # create dictionary where every key is a genre and every value
  # is a list of movies of specific genre
  tmp_genres_dict = {}
  for genre in genres_lst:
    tmp_genres_dict[genre] = []
  for movie_id in movies_dict:
    list_of_genres_for_specific_movie = [genre.lower() for genre in movies_dict[movie_id][2]]
    for genre in list_of_genres_for_specific_movie:
      if genre in genres_lst:
        tmp_genres_dict[genre].append(movies_dict[movie_id])

  # sort movies by rating then by year then by title for every genre
  genres_dict = {}
  for genre in tmp_genres_dict:
    genres_lst_sorted_by_title = sorted(tmp_genres_dict[genre], key=itemgetter(0))
    genres_lst_sorted_by_everything = sorted(genres_lst_sorted_by_title, key=itemgetter(3, 1), reverse=True)
    genres_dict[genre] = genres_lst_sorted_by_everything

  return genres_dict


def top_movies_by_combined_genres(genres_lst, movies_dict):

  """Makes a dictionary where key are genres from genres_lst joined through '&' symbol and value
     is a list of movies containing ALL those genres sorted by rating then by year then by title"""

  # check if movie description contain ALL genres from genres_lst
  movies_lst = []
  for movie_id in movies_dict:
    list_of_genres_for_specific_movie = [genre.lower() for genre in movies_dict[movie_id][2]]
    tmp_lst = []
    for genre in genres_lst:
      tmp_lst.append(genre in list_of_genres_for_specific_movie)
    if all(tmp_lst):  
      movies_lst.append(movies_dict[movie_id])

  # sort
  movies_lst_sorted_by_title = sorted(movies_lst, key=itemgetter(0))
  movies_lst_sorted_by_everything = sorted(movies_lst_sorted_by_title, key=itemgetter(3, 1), reverse=True)    

  combined_genres_dict = {'&'.join(genres_lst):movies_lst_sorted_by_everything}

  return combined_genres_dict


def display_results(movies_dict, n):
  print('genre, title, year, genres, average_rating')
  for genre in movies_dict:
    for movie_description in movies_dict[genre][:n]:
      #title = '"' + movie_description[0] + '"'
      title = movie_description[0].replace(',','')
      year = movie_description[1]
      genres = ' '.join(movie_description[2])	
      average_rating = movie_description[-1] 
      print(genre, title, year, genres, sep = ', ', end=', ')
      
      #for index in range(len(movie_description)-1):
       # print(movie_description[index], end=', ')
      print(average_rating)


if __name__ == '__main__':
  main()
